﻿using System;
using Silanis.BO;

namespace Silanis
{
    class Program
    {

        static void Main(string[] args)
        {
           
            var controller = new AppController(
                new Pot (),
                new Draw()
                );

            while (true)
            {
                Console.WriteLine("Entrer command [purchace | draw | winners] :");
                var command = Console.ReadLine();
                controller.ExecuteCommade(command);
            }

        }
    }
}
