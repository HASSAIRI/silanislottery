﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Silanis.BO;
using Silanis.Services;

namespace Silanis
{
    public class AppController
    {
        private readonly IDrawService _drawService;
        private readonly IPurchaceService _purchaceService;

        private Pot Pot { get; set; }
        private Draw Draw { get; set; }

        public AppController(Pot pot, Draw draw)
        {
            var potService = new PotService();
            potService.InitializePot(200, pot);

            _drawService = new DrawService(potService);
            _purchaceService = new PurchaceService(_drawService);

            Pot = pot;
            Draw = draw;

        }

        public void ExecuteCommade(string command)
        {
            switch (command)
            {
                case "purchace":
                    Purchace();
                    break;
                case "draw":
                    StarDraw();
                    break;
                case "winners":
                    PrintWinners();
                    break;
                default: Console.WriteLine("invalid command");
                    break;
            }
        }

        private void PrintWinners()
        {
            try
            {
                var wins = _drawService.GetWinners(Draw, Pot);
                var display = "1st ball\t2nd ball\t3rd ball\r\n" +
                              string.Join("\t", wins.Select(x => x.Value.FirstName + ": " + x.Key + "$"));

                Console.WriteLine();
                Console.WriteLine(display);
                Console.WriteLine();
            }
            catch (Exception error)
            {

                Console.WriteLine(error.Message);
            }

        }

        private void StarDraw()
        {
            Console.WriteLine("Starting Draw...");
            try
            {
                _drawService.StartDraw(Pot, Draw);
            }
            catch (Exception error)
            {

                Console.WriteLine(error.Message);
            }

            Console.WriteLine("Completed.");
        }

        private void Purchace()
        {
            Console.WriteLine("Please entrer participant first name:");
            var firstName = Console.ReadLine();
            try
            {
                _purchaceService.PurchaceTicket(firstName, Draw);
                Console.WriteLine("{0} registred, {1}$ recieved.\n", firstName, PurchaceService.DefaultPrice);
            }
            catch (Exception error)
            {

                Console.WriteLine(error.Message);
            }

        }
    }
}
