﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Silanis.BO
{
    public class Ticket
    {
        public static decimal Price { get; set; }
        public int Number { get; set; }
        public Participant Participant { get; set; }
    }
}
