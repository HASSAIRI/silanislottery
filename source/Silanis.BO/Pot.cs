﻿namespace Silanis.BO
{
    public class Pot
    {
        public decimal AvailablePrice { get; set; }
        public decimal StartingPrice { get; set; }
    }
}