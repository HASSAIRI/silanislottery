﻿using System.Collections.Generic;

namespace Silanis.BO
{
    public enum EnStatus
    {
        Open,
        Completed
    }

    public class Draw
    {
        public static int MaximunTickets = 50;
        public static decimal[] Prices = new decimal[3] { .75m, .10m, .05m };
        public List<Ticket> Tickets;
        public List<Ticket> WinningTickets;
        public EnStatus Status;

        public Draw()
        {
            Tickets = new List<Ticket>();
            Status = EnStatus.Open;
        }

    }
}
