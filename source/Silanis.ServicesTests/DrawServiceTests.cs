﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Silanis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Silanis.BO;

namespace Silanis.Services.Tests
{
    [TestClass()]
    public class DrawServiceTests
    {
        [TestMethod()]
        public void StartDrawEmptyTest()
        {

            var sub = new Draw();
            var sut = new DrawService(new PotService());
           
            try
            {
                sut.StartDraw(new Pot(), sub);
                Assert.Fail();
            }
            catch (Exception e)
            {
              Assert.IsNotNull(e);
            }
        }


    }
}