﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Silanis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Silanis.BO;

namespace Silanis.Services.Tests
{
    [TestClass()]
    public class PurchaceServiceTests
    {
        [TestMethod()]
        public void PurchaceTicketWhenFullTest()
        {
            var sut = new PurchaceService(new DrawService(new PotService()));
            var mok = new Draw();
            for (int i = 0; i < Draw.MaximunTickets; i++)
            {
                mok.Tickets.Add(new Ticket());
            }

            try
            {
                sut.PurchaceTicket("anyname", mok);
                Assert.Fail();
            }
            catch (Exception e)
            {

                Assert.IsNotNull(e);
            }
        }
    }
}