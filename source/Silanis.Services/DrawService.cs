﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Silanis.BO;

namespace Silanis.Services
{
    public class DrawService : IDrawService
    {
        private readonly IPotService _potService;

        public DrawService(IPotService potService)
        {
            _potService = potService;
        }

        public void StartDraw(Pot pot, Draw draw)
        {
            if (draw.Tickets.Count == 0)
            {
                throw new Exception("Can't start draw because no participants.");
            }

            if (draw.Status == EnStatus.Completed)
            {

                throw new Exception("draw already completed, purchace tickets for next draw");
            }

            draw.WinningTickets = draw.Tickets.OrderBy(elem => Guid.NewGuid()).Take(3).ToList();
            draw.Status = EnStatus.Completed;
            _potService.UpdatePot(pot);
        }

        public Ticket GetNextTicket(Draw draw)
        {
            if (AvailableTickets(draw))
            {
                return new Ticket
                {
                    Number = draw.Tickets.Count == 0 ? 1 : 1 + draw.Tickets.Last().Number
                };

            }
            return null;
        }

        public void RegistrerTicket(Ticket ticket, Draw draw)
        {
            draw.Tickets.Add(ticket);
        }

        private bool AvailableTickets(Draw draw)
        {
            return draw.Tickets.Count < Draw.MaximunTickets;
        }

        public Dictionary<int, Participant> GetWinners(Draw draw, Pot pot)
        {
            if (draw.Status == EnStatus.Open)
            {
                throw new Exception("draw not started yet.");
            }

            var result = new Dictionary<int, Participant>();

            foreach (var ticket in draw.WinningTickets)
            {
                var price = Draw.Prices[draw.WinningTickets.IndexOf(ticket)] * pot.AvailablePrice;
                result.Add(Convert.ToInt32(price), ticket.Participant);
            }

            return result;


        }
    }
}