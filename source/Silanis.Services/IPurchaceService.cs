using Silanis.BO;

namespace Silanis.Services
{
    public interface IPurchaceService
    {
        void PurchaceTicket(string firstName, Draw draw);
    }
}