﻿using System;
using Silanis.BO;

namespace Silanis.Services
{
    public class PurchaceService : IPurchaceService
    {
        private readonly IDrawService _drawService;
        public static decimal DefaultPrice = 10;

       public PurchaceService(IDrawService drawService)
        {
            _drawService = drawService;
        }

        public void PurchaceTicket(string firstName, Draw draw)
        {
            if (draw.Status == EnStatus.Completed)
            {
                draw = new Draw() ;
            }
            var ticket = _drawService.GetNextTicket(draw);

            if (ticket != null)
            {
                ticket.Participant = new Participant
                {
                    FirstName = firstName,
                };

                _drawService.RegistrerTicket(ticket, draw);
                
            }
            else
            {
                throw new Exception("no more tickets");
            }




        }

        
    }
}
