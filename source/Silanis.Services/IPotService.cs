using Silanis.BO;

namespace Silanis.Services
{
    public interface IPotService
    {
        void InitializePot(decimal price, Pot pot);
        void UpdatePot(Pot pot);
    }
}