using System.Collections.Generic;
using Silanis.BO;

namespace Silanis.Services
{
    public interface IDrawService
    {
        void StartDraw(Pot pot, Draw draw);
        Ticket GetNextTicket(Draw draw);
        void RegistrerTicket(Ticket ticket, Draw draw);
        Dictionary<int, Participant>  GetWinners(Draw draw, Pot pot);
    }
}