﻿using Silanis.BO;

namespace Silanis.Services
{
    public class PotService : IPotService
    {
        public void InitializePot(decimal price, Pot pot)
        {
            pot.AvailablePrice = price;
        }

        public void UpdatePot(Pot pot)
        {
            pot.AvailablePrice = pot.AvailablePrice / 2;
        }
    }
}
